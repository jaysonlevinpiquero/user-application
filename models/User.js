const mongoose = require("mongoose");

module.exports = User = mongoose.model(
    "user",
    new mongoose.Schema({
        first_name: { type: String, required: true },
        last_name: { type: String, required: true },
        email: { type: String, required: true, unique: true },
        password: { type: String, required: true },

        billing_address: { type: String },
        delivery_address: { type: String },

        mobile: { type: String, required: true },
        avatar: { type: String },
        date_created: { type: Date, default: Date.now },
    }),
    "user"
);
