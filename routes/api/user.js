const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");


const { check } = require("express-validator");
const { createUser, getAllUsers, editUser } = require("../../controllers/user");

const middleRoute = "User";

router.use(function (req, res, next) {
    console.log(`MIDDLE FUNCTION FOR ${middleRoute}`);
    next();
});

router.get("/", [], getAllUsers);

router.post(
    "/",
    [
        check("first_name", "First Name is required").not().isEmpty(),
        check("last_name", "Last Name is required").not().isEmpty(),
        check("email", "Valid email is required").isEmail(),
        check("mobile", "Mobile Number is required").not().isEmpty(),
        check("password", `6 characters password is required`).isLength({
            min: 6,
        }),
        check(
            "passwordConf",
            `6 characters confirm password is required`
        ).isLength({
            min: 6,
        }),
        check("delivery_address", "Delivery Address is required")
            .not()
            .isEmpty(),
        check("billing_address", "Billing Address is required").not().isEmpty(),
    ],
    createUser
);

router.put(
    "/:id",
    [
        auth,
        check("first_name", "First Name is required").not().isEmpty(),
        check("last_name", "Last Name is required").not().isEmpty(),
        check("email", "Valid email is required").isEmail(),
        check("mobile", "Mobile Number is required").not().isEmpty(),
        check("delivery_address", "Delivery Address is required")
            .not()
            .isEmpty(),
        check("billing_address", "Billing Address is required").not().isEmpty(),
    ],
    editUser
);

module.exports = router;
