# react-application

The application is an interface where a user is able to view the team members and see their basic info and contact details.

The user is able to query the search bar for the name and email of the team members.

![](readme-images/photo-1.png)


The user is also able to filter the results using different fields from the user.
![](readme-images/photo-2.png)

With interactive components
![](readme-images/photo-3.png)

This application also supports email sending and phone calling if you want to contact a team member!
![](readme-images/photo-4.png)
![](readme-images/photo-5.png)


<h4>How to run:</h4>

- git clone https://github.com/levinpqr/react-application.git <b/>
- npm install (or "yarn install" if you are using yarn) <b/>
- npm start (or "yarn start" if you are using yarn) <b/>
