export const navBarLinks = [
    {
        name: "Login",
        url: "/login",
    },
    {
        name: "Register",
        url: "/register",
    },
];

export const navBarLinksLoggedIn = [
    {
        name: "Overview",
        url: "#",
    },
    {
        name: "Disclosures",
        url: "#",
    },
    {
        name: "Investor Relations",
        url: "#",
    },
    {
        name: "News & Advisories",
        url: "#",
    },
    {
        name: "Careers & Opportunities",
        url: "#",
    },
    {
        name: "Help & Support",
        url: "#",
    },
    {
        name: "Sign Out",
        url: "#",
    },
];

export const REDUX = {
    SET_FILTERED_USERS: "SET_FILTERED_USERS",
    SET_USERS: "SET_USERS",
    SET_LOADING: "SET_LOADING",
    OPEN_MODAL: "OPEN_MODAL",
    CLOSE_MODAL: "CLOSE_MODAL",
    OPEN_SNACKBAR: "OPEN_SNACKBAR",
    CLOSE_SNACKBAR: "CLOSE_SNACKBAR",
    OPEN_SIDENAV: "OPEN_SIDENAV",
    CLOSE_SIDENAV: "CLOSE_SIDENAV",
    SET_FILTER: "SET_FILTER",
    CLEAR_FILTER: "CLEAR_FILTER",
    SET_SEARCH: "SET_SEARCH",
    SET_AUTH: "SET_AUTH",
    UNSET_AUTH: "UNSET_AUTH",
    SET_PAGE: "SET_PAGE",
};

export const URLS = {
    HOME: "/",
    PROFILE: "/profile",
    LOGIN: "/login",
    REGISTER: "/register",
};
