import "./styles.css";
import React from "react";
import { Switch, Route } from "react-router-dom";
import Navbar from "./Components/Navbar";

import { REDUX, URLS } from "./enums";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Home from "./Pages/Home";
import Login from "./Pages/Login";
import Register from "./Pages/Register";
import Profile from "./Pages/Profile";
import Loading from "./Components/Loading";
import ModalComponent from "./Components/ModalComponent";
import Snackbar from "./Components/Snackbar";
import Sidenav from "./Components/Sidenav";
import PrivateRoute from "./Components/PrivateRoute";

type AppProps = {
    setUsers: Function;
    setLoading: Function;
    isSnackbarOpen?: boolean;
    isSidenavOpen?: boolean;
    auth: any;
    setAuth: Function;
};

const App = (props: AppProps) => {
    const {
        setUsers,
        setLoading,
        isSnackbarOpen,
        isSidenavOpen,
        auth,
        setAuth,
    } = props;

    React.useEffect(() => {
        const authStorage = window.localStorage.getItem("flexi-app-auth");
        if (authStorage) setAuth(JSON.parse(authStorage));
    }, []);

    return (
        <div className="App">
            <Navbar auth={auth} />
            <Loading />
            <ModalComponent />
            {isSidenavOpen && <Sidenav />}
            {isSnackbarOpen && <Snackbar />}
            <Switch>
                <Route exact path={URLS.HOME} component={Home} />
                <Route exact path={URLS.LOGIN} component={Login} />
                <Route exact path={URLS.REGISTER} component={Register} />
                <PrivateRoute exact path={URLS.PROFILE} component={Profile} />
            </Switch>
        </div>
    );
};

App.propTypes = {
    isSnackbarOpen: PropTypes.bool,
    isSidenavOpen: PropTypes.bool,
    setUsers: PropTypes.func.isRequired,
    setLoading: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    setAuth: PropTypes.func.isRequired,
};

const mapStateToProps = (state: any) => ({
    isSnackbarOpen: state.snackbar.isOpen,
    isSidenavOpen: state.sidenav.isOpen,
    auth: state.auth,
});

const mapDispatchToProps = (dispatch: any) => ({
    setLoading: (flag: boolean) =>
        dispatch({ type: REDUX.SET_LOADING, payload: flag }),
    setUsers: (users: Array<any>) =>
        dispatch({ type: REDUX.SET_USERS, payload: users }),
    setAuth: (payload: any) => dispatch({ type: REDUX.SET_AUTH, payload }),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
