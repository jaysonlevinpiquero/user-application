import axios, { AxiosRequestConfig } from "axios";

const JSON_HEADERS: AxiosRequestConfig = {
    headers: {
        "Content-Type": "application/json",
    },
};

// const API_URL = process.env.REACT_APP_API_URL;
const API_URL = "http://localhost:5000/api/";

export const login = async (payload: any) => {
    return await axios.post(`${API_URL}auth`, payload, JSON_HEADERS);
}