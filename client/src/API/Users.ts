import axios, { AxiosRequestConfig } from "axios";
import { setAuthToken } from "../utils";

// const API_URL = process.env.REACT_APP_API_URL;
const TEMPAPI_URL = "https://jsonplaceholder.typicode.com/";
const JSON_HEADERS: AxiosRequestConfig = {
    headers: {
        "Content-Type": "application/json",
    },
};

// const API_URL = process.env.REACT_APP_API_URL;
const API_URL = "http://localhost:5000/api/";

export const getUsers = async () => {
    return await axios.get(`${API_URL}user`);
};

export const register = async (payload: any) => {
    return await axios.post(`${API_URL}user`, payload, JSON_HEADERS);
};

export const getUserByToken = async (token: string) => {
    setAuthToken(token);
    return await axios.get(`${API_URL}auth`);
};

export const editUser = async (id: string, payload: any, token: string) => {
    setAuthToken(token);
    return await axios.put(`${API_URL}user/${id}`, payload, JSON_HEADERS);
};
