import React from "react";
import PropTypes from "prop-types";
import emailjs from "emailjs-com";
import { connect } from "react-redux";
import { REDUX } from "../enums";
import { getter } from "../utils";

type EmailFormProps = {
    details: any;
    setLoading: Function;
    closeModal: Function;
    callSnackbar: Function;
};

const EmailForm = (props: EmailFormProps) => {
    const { details, setLoading, closeModal, callSnackbar } = props;
    const { name, email } = details;

    const [formData, setFormData] = React.useState({
        name: name,
        email: email,
        subject: "",
        message: "",
    });

    function onChange(e: any) {
        const elementName = getter(e, "target.name", "");
        const elementValue = getter(e, "target.value", "");
        if (!elementName) return;
        setFormData({
            ...formData,
            [elementName]: elementValue,
        });
    }

    async function sendEmail(e: any) {
        e.preventDefault();
        setLoading(true);
        await emailjs
            .sendForm(
                "service_c0nk9sf",
                "template_n2t0qbi",
                e.target,
                "user_ewRwL8gtHtpJDZZFr7fJq"
            )
            .then(
                (result) => {
                    callSnackbar("Email sent");
                    console.log(result.text);
                },
                (error) => {
                    callSnackbar("Failed to send email");
                    console.log(error.text);
                }
            );
        setLoading(false);
        closeModal();
    }

    return (
        <form onSubmit={sendEmail}>
            <label>Name</label>
            <input
                className="contact-form"
                type="text"
                name="name"
                value={formData.name}
                onChange={onChange}
            />
            <label>Email</label>
            <input
                className="contact-form"
                type="email"
                name="email"
                value={formData.email}
                onChange={onChange}
            />
            <label>Subject</label>
            <input
                className="contact-form"
                type="text"
                name="subject"
                value={formData.subject}
                onChange={onChange}
            />
            <label>Message</label>
            <textarea
                name="message"
                value={formData.message}
                onChange={onChange}
            />
            <div style={{ textAlign: "center" }}>
                <input
                    type="submit"
                    value="Send"
                    className="button send-button"
                    style={{ display: "inline-block" }}
                />
            </div>
        </form>
    );
};

EmailForm.propTypes = {
    details: PropTypes.object.isRequired,
    setLoading: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    callSnackbar: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch: any) => ({
    setLoading: (flag: boolean) =>
        dispatch({ type: REDUX.SET_LOADING, payload: flag }),
    closeModal: () => dispatch({ type: REDUX.CLOSE_MODAL }),
    callSnackbar: (msg: string) =>
        dispatch({ type: REDUX.OPEN_SNACKBAR, payload: { message: msg } }),
});

export default connect(null, mapDispatchToProps)(EmailForm);
