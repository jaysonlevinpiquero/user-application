import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilter } from "@fortawesome/free-solid-svg-icons";
import { connect } from "react-redux";
import { REDUX } from "../enums";

type FilterButtonProps = {
    openSidenav: Function;
};

const FilterButton = (props: FilterButtonProps) => {
    const { openSidenav } = props;

    return (
        <span className="filter-icon pointer" onClick={() => openSidenav()}>
            <FontAwesomeIcon icon={faFilter} className="my-floating-icon" />
        </span>
    );
};

FilterButton.propTypes = {
    openSidenav: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch: any) => ({
    openSidenav: () => dispatch({ type: REDUX.OPEN_SIDENAV }),
});

export default connect(null, mapDispatchToProps)(FilterButton);
