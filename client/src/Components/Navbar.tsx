import { navBarLinks, navBarLinksLoggedIn, REDUX, URLS } from "../enums";
import React from "react";
import Accordion from "./Accordion";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useHistory } from "react-router";

type NavbarProps = {
    auth: any;
    unsetAuth: Function;
};

const Navbar = (props: NavbarProps) => {
    const { auth, unsetAuth } = props;

    const history = useHistory();

    const signOut = () => {
        unsetAuth();
        history.push(URLS.LOGIN);
    };

    const isLoggedIn = Boolean(auth && auth.token);

    const linksToShow: any[] = isLoggedIn ? navBarLinksLoggedIn : navBarLinks;

    return (
        <>
            <div className="top-links">
                {linksToShow.map((link) => {
                    if (link.name === "Sign Out") {
                        return (
                            <span
                                key={link.name}
                                className="top-link-item"
                                onClick={signOut}
                            >
                                <a href={link.url} className="top-link-style">
                                    {link.name}
                                </a>
                            </span>
                        );
                    } else {
                        return (
                            <span key={link.name} className="top-link-item">
                                <a href={link.url} className="top-link-style">
                                    {link.name}
                                </a>
                            </span>
                        );
                    }
                })}
            </div>
            <Accordion />
        </>
    );
};

Navbar.prototype = {
    auth: PropTypes.object.isRequired,
    unsetAuth: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch: any) => ({
    unsetAuth: () => dispatch({ type: REDUX.UNSET_AUTH }),
});

export default connect(null, mapDispatchToProps)(Navbar);
