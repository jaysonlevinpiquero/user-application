import React from "react";
import PropTypes from "prop-types";
import TemplateImage from "../template-image.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faMailBulk,
    faPhone,
    faUserEdit,
    faTruck,
    faMoneyBillWave,
} from "@fortawesome/free-solid-svg-icons";
import { connect } from "react-redux";
import { REDUX, URLS } from "../enums";
import { Link, useHistory } from "react-router-dom";

type UserDetailProps = {
    details: any;
    openModal: Function;
    auth: any;
};

const UserDetail = (props: UserDetailProps) => {
    const history = useHistory();

    const { details, openModal, auth } = props;
    const {
        first_name,
        last_name,
        email,
        mobile,
        delivery_address,
        billing_address,
    } = details;

    const name = `${last_name}, ${first_name}`;

    return (
        <div className="profile bg-light">
            <img src={TemplateImage} className="round-img" />
            <div>
                <h3>{name}</h3>
                <span
                    className="pointer icon-link"
                    onClick={() => {
                        openModal({
                            modalType: "EMAIL",
                            userDetails: { email, name },
                        });
                    }}
                >
                    <FontAwesomeIcon icon={faMailBulk} /> <span>{email}</span>
                </span>
                <br />
                <a href={`tel:${mobile}`} className="pointer icon-link">
                    <FontAwesomeIcon icon={faPhone} /> <span>{mobile}</span>
                </a>
                <br />
                {delivery_address && (
                    <>
                        <FontAwesomeIcon icon={faTruck} />{" "}
                        <span>{delivery_address}</span>
                        <br />
                    </>
                )}
                {billing_address && (
                    <>
                        <FontAwesomeIcon icon={faMoneyBillWave} />{" "}
                        <span>{billing_address}</span>
                        <br />
                    </>
                )}
            </div>
            <div style={{ textAlign: "center" }}>
                {auth && auth.email && auth.email === email && (
                    <span
                        style={{ color: "orange", cursor: "pointer" }}
                        onClick={() => history.push(URLS.PROFILE)}
                    >
                        <FontAwesomeIcon
                            icon={faUserEdit}
                            className="globe-icon"
                        />
                        <br />
                        Edit My Profile
                    </span>
                )}
            </div>
        </div>
    );
};

UserDetail.propTypes = {
    details: PropTypes.object.isRequired,
    openModal: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state: any) => ({
    auth: state.auth,
});

const mapDispatchToProps = (dispatch: any) => ({
    openModal: (params: any) =>
        dispatch({ type: REDUX.OPEN_MODAL, payload: params }),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserDetail);
