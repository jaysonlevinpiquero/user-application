import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { REDUX } from "../enums";

type SnackbarProps = {
    message: string;
    closeSnackbar: Function;
};

const Snackbar = (props: SnackbarProps) => {
    const { message, closeSnackbar } = props;

    React.useEffect(() => {
        setTimeout(() => closeSnackbar(), 3000);
    }, []);

    return (
        <div id="snackbar" className="show">
            {message}
        </div>
    );
};

Snackbar.propTypes = {
    message: PropTypes.string.isRequired,
    closeSnackbar: PropTypes.func.isRequired,
};

const mapStateToProps = (state: any) => ({
    message: state.snackbar.message,
});

const mapDispatchToProps = (dispatch: any) => ({
    closeSnackbar: () => dispatch({ type: REDUX.CLOSE_SNACKBAR }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Snackbar);
