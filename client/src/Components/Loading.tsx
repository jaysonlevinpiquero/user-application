import * as React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

type LoadingProps = {
    loading: boolean;
};

const Loading = (props: LoadingProps) => {
    const { loading } = props;
    if (!loading) return null;
    return (
        <div className="loader-background">
            <div className="loader center">
                    <FontAwesomeIcon icon={faSpinner} />
                </div>
        </div>
    );
};

Loading.propTypes = {
    loading: PropTypes.bool.isRequired,
};

const mapStateToProps = (state: any) => ({ loading: state.loading });

export default connect(mapStateToProps)(Loading);
