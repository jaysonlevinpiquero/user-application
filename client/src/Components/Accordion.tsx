import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faUserCheck,
    faCaretUp,
    faCaretDown,
} from "@fortawesome/free-solid-svg-icons";
import { applyUserFilters } from "../utils";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { REDUX, URLS } from "../enums";
import Carousel from "./Carousel";
import { useHistory } from "react-router";

type AccordionProps = {
    setFilteredUsers: Function;
    filterData: any;
    setSearchVal: Function;
    searchVal: string;
    users?: Array<any>;
    auth: any;
    page: string;
};

const Accordion = (props: AccordionProps) => {
    const history = useHistory();

    const {
        users,
        setFilteredUsers,
        filterData,
        setSearchVal,
        searchVal,
        auth,
        page,
    } = props;

    const [isOpen, setIsOpen] = useState(false);

    const toggleAccordion = () => setIsOpen(!isOpen);

    const onSearch = (e: any) => {
        if (!searchVal) return;
        e.preventDefault();
        const usrs = users || [];
        const filteredUsers = applyUserFilters(usrs, searchVal, filterData);
        setFilteredUsers([...filteredUsers]);
    };

    const isLoggedIn = Boolean(auth && auth.token);

    return (
        <div className="accordion">
            <div
                className="accordion-parent"
                style={{
                    boxShadow: "0px 1px 10px #888888",
                }}
            >
                <div className="accordion-parent-left">
                    <FontAwesomeIcon
                        icon={faUserCheck}
                        className="coffee-icon"
                        onClick={() => history.push(URLS.HOME)}
                    />
                    <a className="accordion-link-items" href={URLS.HOME}>
                        Home
                    </a>
                    <a
                        className="accordion-link-items"
                        onClick={toggleAccordion}
                    >
                        See More
                        <FontAwesomeIcon
                            style={{ paddingLeft: "3px" }}
                            icon={isOpen ? faCaretDown : faCaretUp}
                        />
                    </a>
                    {isLoggedIn && (
                        <a className="accordion-link-items" href={URLS.PROFILE}>
                            My Profile
                        </a>
                    )}
                </div>
                <div className="accordion-parent-right">
                    {page === "HOME" && (
                        <>
                            <form className="search-bar" onSubmit={onSearch}>
                                <input
                                    type="text"
                                    placeholder="Search name or email"
                                    name="search"
                                    value={searchVal}
                                    onChange={(e) =>
                                        setSearchVal(e.target.value)
                                    }
                                />
                            </form>
                            <button
                                className="button search-button"
                                onClick={onSearch}
                            >
                                Search
                            </button>
                        </>
                    )}
                </div>
            </div>
            <div className="accordion-content" aria-expanded={!isOpen}>
                <div className="panel">
                    <Carousel />
                </div>
            </div>
        </div>
    );
};

Accordion.propTypes = {
    setFilteredUsers: PropTypes.func.isRequired,
    filterData: PropTypes.object.isRequired,
    setSearchVal: PropTypes.func.isRequired,
    searchVal: PropTypes.string,
    users: PropTypes.array,
    auth: PropTypes.object.isRequired,
    page: PropTypes.string.isRequired,
};

const mapStateToProps = (state: any) => ({
    users: state.usersState.users,
    filterData: state.filter,
    searchVal: state.search,
    auth: state.auth,
    page: state.page,
});

const mapDispatchToProps = (dispatch: any) => ({
    setFilteredUsers: (usrs: any) =>
        dispatch({ type: REDUX.SET_FILTERED_USERS, payload: usrs }),
    setSearchVal: (search: string) =>
        dispatch({ type: REDUX.SET_SEARCH, payload: search }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Accordion);
