import React from "react";
import NatureImage from "../img_nature_wide.jpg";
import SnowImage from "../img_snow_wide.jpg";
import LightsImage from "../img_lights_wide.jpg";

const CAROUSEL_IMAGES = [
    {
        image: NatureImage,
        caption: "Nature Image",
    },
    {
        image: SnowImage,
        caption: "Snow Image",
    },
    {
        image: LightsImage,
        caption: "Lights Image",
    },
];

const Carousel = () => {
    const [slideIndex, setSlideIndex] = React.useState<number>(0);

    function setSlide(n: number) {
        let currIndex: number = n;
        if (currIndex < 0) currIndex = CAROUSEL_IMAGES.length - 1;
        if (currIndex > CAROUSEL_IMAGES.length - 1) currIndex = 0;
        setSlideIndex(currIndex);
    }

    return (
        <>
            <div className="slideshow-container">
                {CAROUSEL_IMAGES.map((item, i) => (
                    <div
                        className={`${
                            i === slideIndex
                                ? "show-carousel-image"
                                : "hide-carousel-image"
                        } fade`}
                        key={i}
                    >
                        <div className="carousel-number-text">
                            {i + 1} / {CAROUSEL_IMAGES.length}
                        </div>
                        <img src={item.image} style={{ width: "100%" }} />
                        <div className="carousel-text">{item.caption}</div>
                    </div>
                ))}

                <a className="prev-button" onClick={() => setSlide(slideIndex - 1)}>
                    &#10094;
                </a>
                <a className="next-button" onClick={() => setSlide(slideIndex + 1)}>
                    &#10095;
                </a>
            </div>
            <br />
            <div style={{ textAlign: "center" }}>
                {CAROUSEL_IMAGES.map((_, i) => (
                    <span
                        className={`carousel-dot ${i === slideIndex ? "active" : ""}`}
                        onClick={() => setSlide(i)}
                        key={i}
                    ></span>
                ))}
            </div>
        </>
    );
};

Carousel.propTypes = {};

export default Carousel;
