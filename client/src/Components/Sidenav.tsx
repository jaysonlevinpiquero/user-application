import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { REDUX } from "../enums";
import { applyUserFilters, getter } from "../utils";

type SidenavProps = {
    closeSidenav: Function;
    setFilter: Function;
    clearFilter: Function;
    setFilteredUsers: Function;
    filterData: any;
    users: Array<any>;
    search?: string;
};

const Sidenav = (props: SidenavProps) => {
    const {
        closeSidenav,
        setFilter,
        clearFilter,
        filterData,
        users,
        search,
        setFilteredUsers,
    } = props;

    const {
        last_name,
        first_name,
        email,
        mobile,
        billing_address,
        delivery_address,
    } = filterData;

    const onChange = (e: any) => {
        const targetName: string = getter(e, "target.name");
        if (!targetName) return;
        const targetValue = getter(e, "target.value", "");
        setFilter({ ...filterData, [targetName]: targetValue });
    };

    const onSubmit = () => {
        closeSidenav();
        const filteredUsers = applyUserFilters(users, search, filterData);
        setFilteredUsers([...filteredUsers]);
    };

    return (
        <div className="sidenav">
            <a className="sidenav-close-button" onClick={() => closeSidenav()}>
                &times;
            </a>
            <div style={{ textAlign: "center" }}>
                <div style={{ display: "inline-block" }}>
                    <h1 className="filter-header">Apply Filter</h1>
                    <br />
                    <label style={{ color: "white" }}>First Name</label>
                    <input
                        className="contact-form"
                        type="text"
                        name="first_name"
                        value={first_name}
                        onChange={onChange}
                    />
                    <label style={{ color: "white" }}>Last Name</label>
                    <input
                        className="contact-form"
                        type="text"
                        name="last_name"
                        value={last_name}
                        onChange={onChange}
                    />
                    <label style={{ color: "white" }}>Email</label>
                    <input
                        className="contact-form"
                        type="text"
                        name="email"
                        value={email}
                        onChange={onChange}
                    />
                    <label style={{ color: "white" }}>Mobile</label>
                    <input
                        className="contact-form"
                        type="text"
                        name="mobile"
                        value={mobile}
                        onChange={onChange}
                    />
                    <label style={{ color: "white" }}>Billing Address</label>
                    <input
                        className="contact-form"
                        type="text"
                        name="billing_address"
                        value={billing_address}
                        onChange={onChange}
                    />
                    <label style={{ color: "white" }}>Delivery Address</label>
                    <input
                        className="contact-form"
                        type="text"
                        name="delivery_address"
                        value={delivery_address}
                        onChange={onChange}
                    />
                    <div className="sidenav-buttons-div">
                        <button
                            className="button search-button"
                            onClick={onSubmit}
                            style={{ marginRight: 10 }}
                        >
                            Filter
                        </button>
                        <button
                            className="button clear-filter-button"
                            onClick={() => clearFilter()}
                            style={{ marginLeft: 10 }}
                        >
                            Clear
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

Sidenav.propTypes = {
    closeSidenav: PropTypes.func.isRequired,
    setFilter: PropTypes.func.isRequired,
    clearFilter: PropTypes.func.isRequired,
    filterData: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired,
    search: PropTypes.string,
    setFilteredUsers: PropTypes.func.isRequired,
};

const mapStateToProps = (state: any) => ({
    filterData: state.filter,
    users: state.usersState.users,
    search: state.search,
});

const mapDispatchToProps = (dispatch: any) => ({
    setFilteredUsers: (usrs: any) =>
        dispatch({ type: REDUX.SET_FILTERED_USERS, payload: usrs }),
    closeSidenav: () => dispatch({ type: REDUX.CLOSE_SIDENAV }),
    setFilter: (obj: any) => dispatch({ type: REDUX.SET_FILTER, payload: obj }),
    clearFilter: () => dispatch({ type: REDUX.CLEAR_FILTER }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidenav);
