import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router-dom";
import { getter } from "../utils";
import { URLS } from "../enums";

type PrivateRouteProps = {
    auth: any;
    loading: boolean;
};

const PrivateRoute = (props: PrivateRouteProps) => {
    const { auth, loading, ...rest } = props;
    let isLoggedIn: boolean = false;
    try {
        const authStorage = JSON.parse(
            window.localStorage.getItem("flexi-app-auth") || ""
        );
        if (authStorage && authStorage.token) isLoggedIn = true;
    } catch (e) {
        console.log(e);
    }
    if (!isLoggedIn) isLoggedIn = Boolean(getter(auth, "token", ""));
    return isLoggedIn || loading ? (
        <Route {...rest} />
    ) : (
        <Redirect to={URLS.HOME} />
    );
};

PrivateRoute.propTypes = {
    auth: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
};

const mapStateToProps = (state: any) => ({
    auth: state.auth,
    loading: state.loading,
});

export default connect(mapStateToProps, null)(PrivateRoute);
