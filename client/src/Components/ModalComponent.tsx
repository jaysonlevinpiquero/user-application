import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { REDUX } from "../enums";
import { getter } from "../utils";
import EmailForm from "./EmailForm";

type ModalComponentProps = {
    modal: any;
    closeModal: Function;
};

const ModalComponent = (props: ModalComponentProps) => {
    const { modal, closeModal } = props;

    const isOpen: boolean = Boolean(getter(modal, "isOpen", false));
    const modalType: string = getter(modal, "modalType", "");
    const userDetails: any = getter(modal, "userDetails", {});

    return (
        <div
            onClick={() => closeModal()}
            className={`modal ${isOpen ? "modal-opened" : "modal-closed"}`}
        >
            <div className="modal-content" onClick={(e) => e.stopPropagation()}>
                <span className="close" onClick={() => closeModal()}>
                    &times;
                </span>
                {modalType === "EMAIL" ? (
                    <EmailForm details={userDetails} />
                ) : (
                    <p>TESTING MODAL..</p>
                )}
            </div>
        </div>
    );
};

ModalComponent.propTypes = {
    modal: PropTypes.object.isRequired,
    closeModal: PropTypes.func.isRequired,
};

const mapStateToProps = (state: any) => ({
    modal: state.modal,
});

const mapDispatchToProps = (dispatch: any) => ({
    closeModal: () => dispatch({ type: REDUX.CLOSE_MODAL }),
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalComponent);
