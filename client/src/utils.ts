import axios from "axios";

export const getter = (obj: any, path: string, defaultValue?: any) => {
    let returnItem = obj;
    try {
        const pathItems: string[] = path.split(".");
        pathItems.forEach((i) => {
            returnItem = returnItem[i];
        });
    } catch (e) {
        console.log(e);
        return defaultValue;
    }
    return returnItem;
};

export const applyUserFilters = (
    users: Array<any>,
    search?: string,
    filterData?: any
) => {
    const filteredUsers = users.filter((usr) => {
        let searchBool: boolean = true;
        let filterBool: boolean = true;
        if (search) {
            searchBool =
                usr.last_name.toLowerCase().includes(search.toLowerCase()) ||
                usr.first_name.toLowerCase().includes(search.toLowerCase()) ||
                usr.email.toLowerCase().includes(search.toLowerCase());
        }
        if (!searchBool) return false;
        if (filterData && Object.keys(filterData).length) {
            for (let key of Object.keys(filterData)) {
                let value: any = filterData[key];
                if (!value) continue;
                if (!usr[key]) return false;
                if (!usr[key].toLowerCase().includes(value.toLowerCase()))
                    return false;
            }
        }
        return true;
    });
    return filteredUsers;
};

export const setAuthToken = (token: string) => {
    if (token) axios.defaults.headers.common["x-auth-token"] = token;
    else delete axios.defaults.headers.common["x-auth-token"];
};
