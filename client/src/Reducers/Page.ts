import { REDUX } from "../enums";
const initialState = "";

export function pageReducer(state = initialState, action: any) {
    const { type, payload } = action;
    switch (type) {
        case REDUX.SET_PAGE:
            return payload;
        default:
            return state;
    }
}
