import { REDUX } from "../enums";
let initState = {
    isOpen: false,
};
export function sidenavReducer(state = initState, action: any) {
    const { type, payload } = action;
    switch (type) {
        case REDUX.OPEN_SIDENAV:
            return { isOpen: true };
        case REDUX.CLOSE_SIDENAV:
            return { isOpen: false };
        default:
            return state;
    }
}
