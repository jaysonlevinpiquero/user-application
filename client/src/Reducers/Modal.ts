import { REDUX } from "../enums";
const initialState = {
    isOpen: false,
    modalType: "",
    userDetails: {},
};

export function modalReducer(state = initialState, action: any) {
    const { type, payload } = action;
    switch (type) {
        case REDUX.OPEN_MODAL:
            return {
                ...state,
                isOpen: true,
                modalType: payload.modalType,
                userDetails: payload.userDetails,
            };
        case REDUX.CLOSE_MODAL:
            return {
                ...state,
                isOpen: false,
                modalType: "",
                userDetails: {},
            };
        default:
            return state;
    }
}
