import { REDUX } from "../enums";
const initialState = {
    email: "",
    token: "",
};

export function authReducer(state = initialState, action: any) {
    const { type, payload } = action;
    switch (type) {
        case REDUX.SET_AUTH:
            return { ...state, ...payload };
        case REDUX.UNSET_AUTH:
            window.localStorage.removeItem("flexi-app-auth");
            return {
                email: "",
                token: "",
            };
        default:
            return state;
    }
}
