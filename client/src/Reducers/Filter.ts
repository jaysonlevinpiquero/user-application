import { REDUX } from "../enums";
const initialState = {
    first_name: "",
    last_name: "",
    email: "",
    mobile: "",
    billing_address: "",
    delivery_address: "",
};

export function filterReducer(state = initialState, action: any) {
    const { type, payload } = action;
    switch (type) {
        case REDUX.SET_FILTER:
            return {
                ...payload,
            };
        case REDUX.CLEAR_FILTER:
            return initialState;
        default:
            return state;
    }
}
