import { REDUX } from "../enums";
let initState = "";
export function searchReducer(state = initState, action: any) {
    const { type, payload } = action;
    switch (type) {
        case REDUX.SET_SEARCH:
            return payload;
        default:
            return state;
    }
}
