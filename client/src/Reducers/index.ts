import { combineReducers } from "redux";
import { userReducer as usersState } from "./Users";
import { loadingReducer as loading } from "./Loading";
import { modalReducer as modal } from "./Modal";
import { snackbarReducer as snackbar } from "./Snackbar";
import { sidenavReducer as sidenav } from "./Sidenav";
import { filterReducer as filter } from "./Filter";
import { searchReducer as search } from "./SearchText";
import { authReducer as auth } from "./Auth";
import { pageReducer as page } from "./Page";

export default combineReducers({
    usersState,
    loading,
    modal,
    snackbar,
    sidenav,
    filter,
    search,
    auth,
    page,
});
