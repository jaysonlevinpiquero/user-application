import { REDUX } from "../enums";
const initialState = {
    users: [],
    filteredUsers: [],
};

export function userReducer(state = initialState, action: any) {
    const { type, payload } = action;
    switch (type) {
        case REDUX.SET_FILTERED_USERS:
            return { ...state, filteredUsers: [...payload] };
        case REDUX.SET_USERS:
            return {
                ...state,
                users: [...payload],
                filteredUsers: [...payload],
            };
        default:
            return state;
    }
}
