import { REDUX } from "../enums";
let initState = {
    isOpen: false,
    message: "",
};
export function snackbarReducer(state = initState, action: any) {
    const { type, payload } = action;
    switch (type) {
        case REDUX.OPEN_SNACKBAR:
            return { isOpen: true, message: payload.message };
        case REDUX.CLOSE_SNACKBAR:
            return { isOpen: false, message: "" };
        default:
            return state;
    }
}
