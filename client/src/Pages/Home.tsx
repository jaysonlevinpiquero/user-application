import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import UserDetail from "../Components/UserDetail";
import FilterButton from "../Components/FilterButton";
import { getUsers } from "../API/Users";
import { REDUX } from "../enums";

type HomeType = {
    users?: Array<any>;
    setLoading: Function;
    setUsers: Function;
    setPage: Function;
};

const Home = (props: HomeType) => {
    const { setLoading, setUsers, users, setPage } = props;

    const initialize = React.useCallback(async () => {
        setLoading(true);

        const res = await getUsers().catch((e) => console.log(e));
        if (!res || !res.data || !Array.isArray(res.data)) return;
        const resultingData = res.data.map((i: any) => ({
            ...i,
        }));
        setUsers([...resultingData]);
        setLoading(false);
    }, [setLoading, getUsers, setUsers]);

    React.useEffect(() => {
        initialize();
        setPage();
    }, []);

    if (!users || !users.length) return <></>;
    const usrList = users.map((usr) => (
        <UserDetail key={usr._id} details={usr} />
    ));
    return (
        <>
            <div style={{ textAlign: "center" }}>
                <div style={{ marginTop: 20, display: "inline-block" }}>
                    {usrList}
                </div>
            </div>
            <FilterButton />
        </>
    );
};

Home.propTypes = {
    users: PropTypes.array,
    setLoading: PropTypes.func.isRequired,
    setUsers: PropTypes.func.isRequired,
    setPage: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch: any) => ({
    setLoading: (flag: boolean) =>
        dispatch({ type: REDUX.SET_LOADING, payload: flag }),
    setUsers: (usrs: any) => dispatch({ type: REDUX.SET_USERS, payload: usrs }),
    setPage: () => dispatch({ type: REDUX.SET_PAGE, payload: "HOME" }),
});

const mapStateToProps = (state: any) => ({
    users: state.usersState.filteredUsers,
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
