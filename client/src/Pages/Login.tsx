import React from "react";
import PropTypes from "prop-types";
import { getter } from "../utils";
import { REDUX, URLS } from "../enums";
import { connect } from "react-redux";
import { login } from "../API/Auth";
import { Redirect } from "react-router";

const Field = React.forwardRef(({ label, type }: any, ref: any) => {
    return (
        <div>
            <label>{label}</label>
            <input ref={ref} type={type} className="contact-form" />
        </div>
    );
});

type LoginProps = {
    auth: any;
    setLoading: Function;
    callSnackbar: Function;
    setAuth: Function;
    setPage: Function;
};

const Login = (props: LoginProps) => {
    const { setLoading, callSnackbar, auth, setAuth, setPage } = props;

    const [isRedirect, setRedirect] = React.useState(false);

    const emailRef = React.useRef();
    const passwordRef = React.useRef();

    React.useEffect(() => {
        setPage();
    }, []);

    const onSubmit = async (data: any) => {
        setLoading(true);
        await login(data)
            .then((res) => {
                setLoading(false);
                const email = getter(emailRef, "current.value", "");
                const authObj = { token: getter(res, "data.token"), email };
                setAuth(authObj);
                window.localStorage.setItem(
                    "flexi-app-auth",
                    JSON.stringify(authObj)
                );
                setRedirect(true);
            })
            .catch((e) => {
                console.log(e);
                callSnackbar(
                    getter(getter(e, "response.data.errors")[0], "msg")
                );
            });
        setLoading(false);
    };

    const handleSubmit = (e: any) => {
        e.preventDefault();
        const data = {
            email: getter(emailRef, "current.value", ""),
            password: getter(passwordRef, "current.value", ""),
        };
        onSubmit(data);
    };

    if ((auth && auth.token) || isRedirect) return <Redirect to={URLS.HOME} />;

    return (
        <>
            <div style={{ paddingBottom: "60px" }}></div>
            <form className="login-form-styles" onSubmit={handleSubmit}>
                <Field ref={emailRef} label="Email:" type="email" />
                <Field ref={passwordRef} label="Password:" type="password" />
                <div style={{ textAlign: "center" }}>
                    <button className="button search-button" type="submit">
                        Login
                    </button>
                </div>
            </form>
        </>
    );
};

Login.propTypes = {
    setLoading: PropTypes.func.isRequired,
    callSnackbar: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    setAuth: PropTypes.func.isRequired,
    setPage: PropTypes.func.isRequired,
};

const mapStateToProps = (state: any) => ({
    auth: state.auth,
});

const mapDispatchToProps = (dispatch: any) => ({
    setLoading: (flag: boolean) =>
        dispatch({ type: REDUX.SET_LOADING, payload: flag }),
    callSnackbar: (msg: string) =>
        dispatch({ type: REDUX.OPEN_SNACKBAR, payload: { message: msg } }),
    setAuth: (payload: any) => dispatch({ type: REDUX.SET_AUTH, payload }),
    setPage: () => dispatch({ type: REDUX.SET_PAGE, payload: "LOGIN" }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
