import React from "react";
import PropTypes from "prop-types";
import { getter } from "../utils";
import { REDUX, URLS } from "../enums";
import { connect } from "react-redux";
import { register } from "../API/Users";
import { Redirect } from "react-router";

type RegisterProps = {
    callSnackbar: Function;
    setLoading: Function;
    setAuth: Function;
    auth: any;
    setPage: Function;
};

const Register = (props: RegisterProps) => {
    const { callSnackbar, setLoading, setAuth, auth, setPage } = props;

    const [isRedirect, setRedirect] = React.useState(false);

    React.useEffect(() => {
        setPage();
    }, []);

    const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const name = getter(e, "target.name", "");
        const value = getter(e, "target.value", "");
        if (!name) return;
        setFormData({ ...formData, [name]: value });
    };

    const [formData, setFormData] = React.useState({
        first_name: "",
        last_name: "",
        email: "",
        mobile: "",
        billing_address: "",
        delivery_address: "",
        password: "",
        passwordConf: "",
    });

    const {
        first_name,
        last_name,
        email,
        mobile,
        billing_address,
        delivery_address,
        password,
        passwordConf,
    } = formData;

    const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        setLoading(true);
        await register(formData)
            .then((res) => {
                setLoading(false);
                const authObj = { token: getter(res, "data.token"), email };
                setAuth(authObj);
                window.localStorage.setItem(
                    "flexi-app-auth",
                    JSON.stringify(authObj)
                );
                setRedirect(true);
            })
            .catch((e) => {
                setLoading(false);
                console.log(e);
                callSnackbar(
                    getter(getter(e, "response.data.errors")[0], "msg")
                );
            });
    };

    if ((auth && auth.token) || isRedirect) return <Redirect to={URLS.HOME} />;

    return (
        <>
            <div style={{ paddingBottom: "20px" }}></div>
            <form className="register-form-styles" onSubmit={onSubmit}>
                <div className="grid">
                    <div className="first-name-text">
                        <label>First Name</label>
                        <input
                            type="text"
                            className="contact-form"
                            name="first_name"
                            value={first_name}
                            onChange={onChange}
                        />
                    </div>
                    <div className="last-name-text">
                        <label>Last Name</label>
                        <input
                            type="text"
                            className="contact-form"
                            name="last_name"
                            value={last_name}
                            onChange={onChange}
                        />
                    </div>

                    <div className="email-text">
                        <label>Email</label>
                        <input
                            type="email"
                            className="contact-form"
                            name="email"
                            value={email}
                            onChange={onChange}
                        />
                    </div>

                    <div className="mobile-number-text">
                        <label>Mobile Number</label>
                        <input
                            type="text"
                            className="contact-form"
                            name="mobile"
                            value={mobile}
                            onChange={onChange}
                        />
                    </div>

                    <div className="delivery-address-text">
                        <label>Delivery Address</label>
                        <input
                            type="text"
                            className="contact-form"
                            name="delivery_address"
                            value={delivery_address}
                            onChange={onChange}
                        />
                    </div>
                    <div className="billing-address-text">
                        <label>Billing Address</label>
                        <input
                            type="text"
                            className="contact-form"
                            name="billing_address"
                            value={billing_address}
                            onChange={onChange}
                        />
                    </div>
                    <div className="password-text">
                        <label>Password</label>
                        <input
                            type="password"
                            className="contact-form"
                            name="password"
                            value={password}
                            onChange={onChange}
                        />
                    </div>
                    <div className="conf-password-text">
                        <label>Confirm Password</label>
                        <input
                            type="password"
                            className="contact-form"
                            name="passwordConf"
                            value={passwordConf}
                            onChange={onChange}
                        />
                    </div>
                </div>

                <div style={{ textAlign: "center" }}>
                    <button className="button search-button" type="submit">
                        Sign up
                    </button>
                </div>
            </form>
        </>
    );
};

Register.propTypes = {
    callSnackbar: PropTypes.func.isRequired,
    setLoading: PropTypes.func.isRequired,
    setAuth: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    setPage: PropTypes.func.isRequired,
};

const mapStateToProps = (state: any) => ({
    auth: state.auth,
});

const mapDispatchToProps = (dispatch: any) => ({
    setLoading: (flag: boolean) =>
        dispatch({ type: REDUX.SET_LOADING, payload: flag }),
    callSnackbar: (msg: string) =>
        dispatch({ type: REDUX.OPEN_SNACKBAR, payload: { message: msg } }),
    setAuth: (payload: any) => dispatch({ type: REDUX.SET_AUTH, payload }),
    setPage: () => dispatch({ type: REDUX.SET_PAGE, payload: "REGISTER" }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
