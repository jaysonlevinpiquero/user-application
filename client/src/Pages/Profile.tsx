import React from "react";
import PropTypes from "prop-types";
import { getter } from "../utils";
import { REDUX, URLS } from "../enums";
import { connect } from "react-redux";
import { getUserByToken, editUser } from "../API/Users";

type ProfileProps = {
    callSnackbar: Function;
    setLoading: Function;
    setAuth: Function;
    auth: any;
    setPage: Function;
};

const Profile = (props: ProfileProps) => {
    const { callSnackbar, setLoading, setAuth, auth, setPage } = props;

    const initialize = React.useCallback(async () => {
        if (!auth || !auth.token) return;
        const token = auth && auth.token ? auth.token : "";
        setLoading(true);
        await getUserByToken(token)
            .then((res) => {
                setFormData({ ...res.data });
            })
            .catch((e) => {
                setLoading(false);
                console.log(e);
                callSnackbar(getter(e, "response.data.msg"));
            });
        setLoading(false);
    }, [setLoading, auth]);

    React.useEffect(() => {
        setPage();
        if (!auth || !auth.token) return;
        initialize();
    }, [initialize, auth]);

    const [formData, setFormData] = React.useState({
        first_name: "",
        last_name: "",
        email: "",
        mobile: "",
        billing_address: "",
        delivery_address: "",
        _id: "",
    });

    const {
        first_name,
        last_name,
        email,
        mobile,
        billing_address,
        delivery_address,
        _id,
    } = formData;

    const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const name = getter(e, "target.name", "");
        const value = getter(e, "target.value", "");
        if (!name) return;
        setFormData({ ...formData, [name]: value });
    };

    const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        setLoading(true);
        const token = auth && auth.token ? auth.token : "";
        await editUser(
            _id,
            {
                first_name,
                last_name,
                email,
                mobile,
                billing_address,
                delivery_address,
            },
            token
        )
            .then((res) => {
                callSnackbar("Update successful");
            })
            .catch((e) => {
                setLoading(false);
                console.log(e);
                callSnackbar(
                    getter(getter(e, "response.data.errors")[0], "msg")
                );
            });
        setLoading(false);
    };

    return (
        <>
            <div style={{ paddingBottom: "20px" }}></div>
            <form className="register-form-styles" onSubmit={onSubmit}>
                <div className="grid">
                    <div className="first-name-text">
                        <label>First Name</label>
                        <input
                            type="text"
                            className="contact-form"
                            name="first_name"
                            value={first_name}
                            onChange={onChange}
                        />
                    </div>
                    <div className="last-name-text">
                        <label>Last Name</label>
                        <input
                            type="text"
                            className="contact-form"
                            name="last_name"
                            value={last_name}
                            onChange={onChange}
                        />
                    </div>

                    <div className="email-text">
                        <label>Email</label>
                        <input
                            type="email"
                            className="contact-form"
                            name="email"
                            value={email}
                            onChange={onChange}
                        />
                    </div>

                    <div className="mobile-number-text">
                        <label>Mobile Number</label>
                        <input
                            type="text"
                            className="contact-form"
                            name="mobile"
                            value={mobile}
                            onChange={onChange}
                        />
                    </div>

                    <div className="delivery-address-text">
                        <label>Delivery Address</label>
                        <input
                            type="text"
                            className="contact-form"
                            name="delivery_address"
                            value={delivery_address}
                            onChange={onChange}
                        />
                    </div>
                    <div className="billing-address-text">
                        <label>Billing Address</label>
                        <input
                            type="text"
                            className="contact-form"
                            name="billing_address"
                            value={billing_address}
                            onChange={onChange}
                        />
                    </div>
                </div>

                <div style={{ textAlign: "center" }}>
                    <button className="button search-button" type="submit">
                        Save
                    </button>
                </div>
            </form>
        </>
    );
};

Profile.propTypes = {
    callSnackbar: PropTypes.func.isRequired,
    setLoading: PropTypes.func.isRequired,
    setAuth: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state: any) => ({
    auth: state.auth,
});

const mapDispatchToProps = (dispatch: any) => ({
    setLoading: (flag: boolean) =>
        dispatch({ type: REDUX.SET_LOADING, payload: flag }),
    callSnackbar: (msg: string) =>
        dispatch({ type: REDUX.OPEN_SNACKBAR, payload: { message: msg } }),
    setAuth: (payload: any) => dispatch({ type: REDUX.SET_AUTH, payload }),
    setPage: () => dispatch({ type: REDUX.SET_PAGE, payload: "PROFILE" }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
