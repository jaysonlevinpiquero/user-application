# User Application

A great application for exploration and interconnection! This user application allows us to register, login, view, and contact other members of this app! This app uses Express for the Backend and ReactJS with Typescript for the Frontend.


With the elegant login page, we can sign in using our email addresses.
![Alt text](/readme-images/login.png "Optional Title")



Or register if you have not already!
![Alt text](/readme-images/register.png "Optional Title")


We can explore and contact other members in our home page
![Alt text](/readme-images/home.png "Optional Title")


Search them if this list is too long with our interactive search bar
![Alt text](/readme-images/search.png "Optional Title")


Or filter them out using specific fields such as name and location!
![Alt text](/readme-images/filter.png "Optional Title")


We can contact other members by email or by phone by clicking on the email/phone link on this list!
![Alt text](/readme-images/send-email.png "Optional Title")


By clicking on the edit icon, we will be redirected to our own profile edit page where we can update any information necessary
![Alt text](/readme-images/edit-info.png "Optional Title")


And finally, we can enjoy the view with our interactive UI!
![Alt text](/readme-images/accordion.png "Optional Title")




# Getting Started

To start development, clone this repository:
https://gitlab.com/jaysonlevinpiquero/user-application.git

Once successful, make sure we have used the same NodeJS version to avoid any conflicts. For this project, node version 10.15.3 was used. Please adjust yours accordingly.

cd into the cloned repository and run the bin/start.sh script: **source bin/start.sh**

This specifies the node version and installs the fontend and backend dependencies of our application. Once successful, we can concurrently start our backend and frontend applications with: 

**npm run dev**

OR

**yarn run dev** (for those using yarn)

This will open a browser to run our application! Finally, enjoy with development!
